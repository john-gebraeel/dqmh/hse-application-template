# HAMPEL SOFTWARE ENGINEERING's Windows Application Template

A demo Windows application based on our UI framework, which builds on the [HSE DQMH flavor](https://dokuwiki.hampel-soft.com/code/dqmh/hse-flavour) of the Delacor Queued Message Handler (DQMH, www.delacor.com) and facilitates our [HSE Libraries](https://dokuwiki.hampel-soft.com/code/hse-libraries). With basic functionality for module loading and configuration, UI management and logging/debugging.

Features include:
* Dynamic loading of DQMH modules (no static references to UI Framework code)
* User Interface module for managing runtime menus, subpanels etc.
* Navigation module for displaying dynamically created harmonica menus
* Event Manager module for showing real-time debug information 

UI Framework and HSE Libraries bring even more goodness, eg:
* Dynamic module-to-module communication through generic broadcasts ("System Messages")
* Low-level logging through the HSE Logger 
* and many more!

## :bulb: Documentation

The HSE Dokuwiki holds more information:

* https://dokuwiki.hampel-soft.com/code/dqmh/hse-application-template
* https://dokuwiki.hampel-soft.com/code/dqmh/hse-flavour
* https://dokuwiki.hampel-soft.com/code/hse-libraries

### :question: FAQ
See our [FAQ](https://dokuwiki.hampel-soft.com/code/dqmh/hse-application-template/faq) for comments on updating versions amongst other things.

## :rocket: Installation

See our detailed guide on https://dokuwiki.hampel-soft.com/code/dqmh/hse-application-template/setup

### :link: Dependencies

Apply the VI Package Configuration File from `/hse-application-template.vipc`

### :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016.


## :busts_in_silhouette: Contributing 

The Modules and Module Templates in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com).

Please get in touch with us at (office@hampel-soft.com) or visit our website (www.hampel-soft.com) if you want to contribute.

##  :beers: Credits

* Joerg Hampel
* Manuel Sebald
* Bence Bartho
* Danny Thomson
* John Gebraeel
* Benjamin Hinrichs
* Julian Schary

## :page_facing_up: License 

This repository and its contents are licensed under a BSD/MIT like license - see the [LICENSE](LICENSE) file for details
